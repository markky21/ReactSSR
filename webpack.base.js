const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path')


module.exports = {

    // resolve: {
    //     extensions: ['.js', '.css']
    // },

    // Loaders
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: [
                        'react',
                        'stage-0',
                        ['env', {targets: {browsers: ['last 2 versions']}}]
                    ]
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            query: {
                                localIdentName: '[hash:8]',
                                modules: true
                            }
                        },
                        {
                            loader: 'postcss-loader'
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                includePaths: [path.resolve(__dirname, 'node_modules')],
                            },
                        }
                    ]
                })
            }
        ]
    },

    plugins: [
    new ExtractTextPlugin({
        filename: '[name].css',
        allChunks: true
    })
]
}