import React from 'react';
import styles from './HomePage.scss';



const Home = () => {

    return (
        <div className="center-align" style={{marginTop: '50px'}}>
            <h3>Welcome</h3>
            <p>Check out these awesome features</p>

            <div className={styles.wrapper}>
                <button className={styles.button}>
                    Click Me
                </button>
            </div>

        </div>
    )
};

export default {
    component: Home
};