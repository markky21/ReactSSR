import React from 'react';

const Page404 = ({staticContext = {}}) => {

    staticContext.notFound = true;

    return (
        <article className="center-align" style={{marginTop: '100px'}}>
            <h2>Page not found. 404</h2>
        </article>
    );

}

export default {
    component: Page404
}