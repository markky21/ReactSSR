import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchUsers} from "../actions";

import {Helmet} from 'react-helmet'

class UsersList extends Component {

    componentDidMount() {
        this.props.fetchUsers();
    }

    renderUsers() {
        return this.props.users.map(user => {
            return <li key={user.id}>{user.name}</li>;
        });
    }

    head() {
        return (
            <Helmet>
                <title>{`${this.props.users.length} Users Loaded`}</title>
                <meta property="og:title" content="Users App"/>
            </Helmet>
        );
    }

    render() {
        return (
            <div className='row' style={{marginTop: '50px'}}>
                {this.head()}
                <div className='col'>
                    <h5>Here's a big list of users:</h5>
                    <ul>{this.renderUsers()}</ul>
                </div>
            </div>
        );
    }

}

function mapStateToProps({users}) {
    return {users}
}

// // Można sworzyć funkcję lub bezpośrednio w loadData wpisać funkcję strzałkową
// function loadData(store) {
//     return store.dispatch(fetchUsers());
// }

export default {
    component: connect(mapStateToProps, {fetchUsers})(UsersList),
    loadData: ({dispatch}) => dispatch(fetchUsers())
}