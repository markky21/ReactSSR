import React, {Component} from 'react';
import {connect} from 'react-redux';

import {fetchAdmins} from '../actions/index';

import requireAuth from '../components/hocs/requireAuth';


class AdminPage extends Component {

    componentDidMount() {
        this.props.fetchAdmins();
    }

    renderAdmins() {
        return this.props.admins.map((admin) => {
            return <li key={admin.id}>{admin.name}</li>;
        });

    }

    render() {
        return (
            <div className='row' style={{marginTop: '50px'}}>
                <div className='col'>
                    <h5>Here is a list of admins:</h5>
                    <ul>{this.renderAdmins()}</ul>
                </div>
            </div>
        );
    }

}

function loadData(store) {
    return store.dispatch(fetchAdmins());
}

function mapStateToProps({admins}) {
    return {admins}
}

export default {
    component: connect(mapStateToProps, {fetchAdmins})(requireAuth(AdminPage)),
    loadData
}