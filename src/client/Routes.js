import React from 'react';
// import {Route} from 'react-router-dom';

import App from './App';
import HomePage from './pages/HomePage';
import UsersListPage from './pages/UsersListPage';
import AdminsPage from './pages/AdminsPage';
import Page404 from './pages/Page404';

/*
// Klasyczny sposób zapisywania Routingu
export default () => {
    return (
        <div>
            <Route exact path="/" component={Home}/>
            <Route path="/users" component={UsersList}/>
        </div>
    );
};
*/

// react-router-config wymaga zapisu tablicoweo routingu
export default [
    {
        ...App,
        routes: [
            {
                ...HomePage,
                path: '/',
                exact: true
            },
            {
                ...UsersListPage,
                path: '/users'
            },
            {
                ...AdminsPage,
                path: '/admins'
            },
            {
                ...Page404,
                path: '*'
            }
        ]
    }
]