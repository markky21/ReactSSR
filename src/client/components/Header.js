import React from 'react';
import {Link} from 'react-router-dom';

import {connect} from 'react-redux';

const Header = ({auth}) => {
    console.log('my auth state is: ', auth);

    const authButton = auth ?
        (<a href="/api/logout">Logout</a>)
        :
        (<a href="/api/auth/google">Log in</a>);

    return (
        <nav className='root'>
            <div className="nav-wrapper">
                <Link className="brand-logo left" to="/">React SSR</Link>
                <ul className="right">
                    <li><Link to="/users">Users</Link></li>
                    <li><Link to="/admins">Admins</Link></li>
                    <li>{authButton}</li>
                </ul>
            </div>
        </nav>
    );
}

function mapStateToProps({auth}) {
    return {auth}
}

export default connect(mapStateToProps)(Header)