// Startup point for ther client side aplication
import 'babel-polyfill'; // RESOLVE Error: Uncaught ReferenceError: regeneratorRuntime is not defined (użycie async await)

import React from 'react';
import ReactDOM from 'react-dom';

import {createStore, applyMiddleware, compose} from 'redux';
import reducers from './reducers/index';
import thunk from 'redux-thunk';
import axios from 'axios';

import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import {renderRoutes} from "react-router-config";
import Routes from './Routes';


const axiosInstansce = axios.create({
    baseURL: '/api',
}); // Ustawiamy predefiniowaną instancje axios i przekażemy ją jako ekstra argument do redux thunk. W każdym actionCreator musimy przekazać tą wartość. Patrz acion creators

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    reducers,
    window.INITIAL_STATE,
    composeEnhancers(
        applyMiddleware(thunk.withExtraArgument(axiosInstansce))
    )
); // window.INITIAL_STATE === store przekazany w index.js servera

ReactDOM.hydrate( // hydrate działa podobnie jak render tylko jest używana dla elementów SSR
    <Provider store={store}>
        <BrowserRouter>
            <div>{renderRoutes(Routes)}</div>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'));