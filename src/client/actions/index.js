export const FETCH_USERS = 'fetch_users'; // można pomyśleć o wydzieleniu wszystkich stałych type do osobnego pliku
export const FETCH_ADMINS = 'fetch_admins';
export const FETCH_CURRENT_USER = 'fetch_current_user';


export const fetchUsers = () => async (dispatch, getState, api) => { // api <--- axiosInstance

    const res = await api.get('/users'); // server odbierze ten request i przekaże do serwera api

    dispatch({
        type: FETCH_USERS,
        payload: res
    })
};


export const fetchCurrentUser = () => async (dispatch, getState, api) => {

    const res = await api.get('/current_user');

    dispatch({
        type: FETCH_CURRENT_USER,
        payload: res
    })

};

export const fetchAdmins = () => async (dispatch, getState, api) => {

    const res = await api.get('/admins');

    dispatch({
        type: FETCH_ADMINS,
        payload: res
    });

}