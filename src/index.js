import 'babel-polyfill'; // RESOLVE Error: Uncaught ReferenceError: regeneratorRuntime is not defined (użycie async await)
import express from 'express';
import proxy from 'express-http-proxy';
import renderer from './helpers/renderer';
import createStore from './helpers/createStore';
import {matchRoutes} from 'react-router-config';
import Routes from './client/Routes';

const app = express();

app.use(
    '/api',
    proxy('http://react-ssr-api.herokuapp.com', {
        proxyReqOptDecorator(opts) {  // Dodatkowe ustawienia wynikają tylko ze sposobu konfiguracji servera api. Normalnie nie jest to konieczne
            opts.headers['x-forwarded-host'] = 'localhost:3000';
            return opts;
        }
    })
);

app.use(express.static('public')); // ustawienie dostepnego folderu 'public' dla wszystkich
app.get('*', (req, res) => {
    const store = createStore(req);


    // Some logic to initialize and load data into the store

    // Dzięki 'matchRoutes' wiemy jakie komponenty będą odpalone
    const promises = matchRoutes(Routes, req.path).map(({route}) => {
        return route.loadData ? route.loadData(store) : null;
    }).map(promise => { // tworzymy nową promiskę tylko dlatego by zrenderować dopiero wtedy kiedy wszystkie promiski wyżej wykonają się lub zostaną odrzucone
        if (promise) {
            return new Promise((resolve, reject) => {
                promise.then(resolve).catch(resolve);
            });
        }
    });

    Promise.all(promises)
        .then(() => {

            const context = {};
            const content = renderer(req, store, context);

            if (context.url) { // jeżeli wykonaliśmy Redirect to wykrywamy to i przekierowujemy po stronie serwera
               return res.redirect(301, context.url);
            }

            if (context.notFound) {
                res.status(404);
            }

            res.send(content);

        });

});

app.listen(3000, () => {
    console.log('Listening on port 3000');
});

